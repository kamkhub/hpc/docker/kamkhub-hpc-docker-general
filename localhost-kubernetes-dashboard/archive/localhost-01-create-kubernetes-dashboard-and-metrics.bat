kubectl config use-context docker-desktop
::kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.5.0/aio/deploy/recommended.yaml
kubectl delete clusterrolebinding kubernetes-dashboard
kubectl apply -f localhost-kubernetes-dashboard-v2.5.0.yaml

:: Install Metrics Server
:: https://github.com/kubernetes-sigs/metrics-server/
:: kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
:: Patch the metrisc server to work with insecure TLS
:: https://dev.to/docker/enable-kubernetes-metrics-server-on-docker-desktop-5434
:: kubectl patch deployment metrics-server -n kube-system --type 'json' -p '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--kubelet-insecure-tls"}]'
:: kubectl apply -f localhost-kubernetes-mertics-server-v0.6.1.yaml

::kubectl get namespaces  --show-labels
kubectl proxy

:: Disabling the login prompt in Kubernetes Dashboard
:::: kubectl patch deployment kubernetes-dashboard -n kubernetes-dashboard --type 'json' -p '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--enable-skip-login"}]'

:: To access Dashboard from your local workstation you must create a secure channel to your Kubernetes cluster. Run the following command:
:::: kubectl proxy
:::: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
