:: Create Nginx Ingress named ingress-test
kubectl config use-context docker-desktop
kubectl create -f k8s-ingress-test.yaml

kubectl wait --namespace ingress-test --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=120s

:: Delete Nginx Ingress
:: kubectl delete -f k8s-ingress-test.yaml
:: kubectl delete namespace ingress-test

:: helm upgrade --install ingress-test ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-test --create-namespace
