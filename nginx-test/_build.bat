:: Build an image from a Dockerfile to GitLab Registry

:: Version 1
:: docker build -f nginx-test1.Dockerfile -t registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test1 .
:: docker build -f nginx-test2.Dockerfile -t registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test2 .

:: Version 2
docker build -f nginx-test1.Dockerfile -t registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test1:ver2 .
docker build -f nginx-test2.Dockerfile -t registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test2:ver2 .
