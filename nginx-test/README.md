# nginx-test

## Testing nginx-test1

docker run -d -p 127.0.0.1:8881:80/tcp --name nginx-test1 registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test1

http://localhost:8881

docker stop nginx-test1

docker container prune -f

## Testing nginx-test2

docker run -d -p 127.0.0.1:8882:80/tcp --name nginx-test2 registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test2

http://localhost:8882

docker stop nginx-test2

docker container prune -f

## Testing port 443 whether it is available in the host

docker run -d -p 127.0.0.1:443:80/tcp --name nginx-test1 registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test1

http://localhost:443

docker stop nginx-test1

docker container prune -f
