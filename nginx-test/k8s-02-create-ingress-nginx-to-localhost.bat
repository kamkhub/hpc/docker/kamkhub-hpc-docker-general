:: Create Nginx Ingress named ingress-test
kubectl config use-context docker-desktop
kubectl create -f k8s-ingress-nginx.yaml

kubectl wait --namespace ingress-nginx --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=120s

:: Delete Nginx Ingress
:: kubectl delete -f k8s-ingress-nginx.yaml

:: helm upgrade --install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace
