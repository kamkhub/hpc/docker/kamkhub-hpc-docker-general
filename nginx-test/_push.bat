:: Push the image to GitLab

:: Version 1
:: docker push registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test1
:: docker push registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test2

:: Version 2
docker push registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test1:ver2
docker push registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test2:ver2
