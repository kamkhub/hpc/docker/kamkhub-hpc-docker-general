kubectl config use-context docker-desktop
kubectl get deployments -n ingress-nginx
kubectl get pods -n ingress-nginx
kubectl describe pods -n ingress-nginx

:: Update images of two deployment
kubectl set image deployment/nginx-test1 container-nginx-test1=registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test1:ver2 -n ingress-nginx
kubectl set image deployment/nginx-test2 container-nginx-test2=registry.gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-general/nginx-test2:ver2 -n ingress-nginx
